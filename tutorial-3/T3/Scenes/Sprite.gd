extends Sprite

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var idle_texture = preload("res://Assets/kenney_platformercharacters/PNG/Adventurer/Poses/adventurer_idle.png")
var walk_texture = preload("res://Assets/kenney_platformercharacters/PNG/Adventurer/Poses/adventurer_walk1.png")
var jump_texture = preload("res://Assets/kenney_platformercharacters/PNG/Adventurer/Poses/adventurer_jump.png")
var char_tex = idle_texture

# Called when the node enters the scene tree for the first time.
func _ready():
	set_texture(idle_texture)

func idle_sprite():
	set_texture(idle_texture)

func jump_sprite():
	set_texture(jump_texture)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
