extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (int) var speed = 400
export (int) var jump_speed = -600

export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()
var jump_count = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('up') and jump_count < 2:
		velocity.y = jump_speed
		jump_count += 1
		$Sprite.jump_sprite()
	if is_on_floor():
		jump_count = 1
		$Sprite.idle_sprite()
		if Input.is_action_just_pressed('up'):
			velocity.y = jump_speed
			$Sprite.jump_sprite()
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
